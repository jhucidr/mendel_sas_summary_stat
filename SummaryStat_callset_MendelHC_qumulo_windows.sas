options error=1 nonotes;

/***
%let NumCallset=%scan(&sysparm,1,"|");
%let NumMendel=%scan(&sysparm,2,"|");
%let NumHapMap=%scan(&sysparm,3,"|");
***/

%let NumCallset=2862;
%let NumMendel=2763;
%let NumHapMap=99;

*************************************
*									*
*		Get Stats from Vcftools		*
*									*
************************************;



*************************************************************
*															*
*				Separate SNV and INDELs						*
* 	for SNV, make all 4 nucleotide info available
*	for INDELs, make the 2 most freq nucleotide info availabe 
*															*
*************************************************************;
options error=1;
%macro GetSNV(Type,SampleSet);
data Count2;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\counts2.frq.count" 
		dlm='09'x dsd firstobs=2 missover;
	length chrpos $ 20 chr $ 8;
	input chr $ pos N_Allele N_Chr Ct_Allele1-Ct_Allele4;
	chrpos=cats(chr,"_",pos);
	Ct_largest1=largest(1,of Ct_Allele1-Ct_Allele4);
	Ct_largest2=largest(2,of Ct_Allele1-Ct_Allele4);
	if Ct_Allele3^=. then BiAllelic=0;
		else if Ct_Allele3=. then BiAllelic=1;
	index=_n_;
	run;
	

data Freq;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\freq.frq" 
		dlm='09'x dsd firstobs=2 missover;
	length chrpos $ 20 chr $ 8 var1-var4 $ 20 Allele1-Allele4 $ 8;
	input chr $ pos N_Allele N_Chr var1-var4 $;
	freq_Allele1=input(scan(var1,2,":"),8.6);freq_Allele2=input(scan(var2,2,":"),8.6);freq_Allele3=input(scan(var3,2,":"),8.6);freq_Allele4=input(scan(var4,2,":"),8.6);
	Allele1=scan(var1,1,":");Allele2=scan(var2,1,":");Allele3=scan(var3,1,":");Allele4=scan(var4,1,":");
	chrpos=cats(chr,"_",pos);
	Freq_Largest1=largest(1,of Freq_Allele1-Freq_Allele4);
	Freq_Largest2=largest(2,of Freq_Allele1-Freq_Allele4);
	drop N_Allele N_Chr var1-var4;
	index=_n_;
	run;

data HWE; /* For BiAllelic variants only */
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\hardy.hwe" 
		dlm='09'x dsd firstobs=2 missover;
	length chrpos $ 20 chr $ 8 Obs_GT_Pre $ 20 Obs_GT $ 20 Exp_GT $ 20;
	input chr $ pos Obs_GT_Pre $ Exp_GT $ Chisq_HWE P_HWE;
	chrpos=cats(chr,"_",pos);
	Ct_Allele1=2*input(scan(Obs_GT,1,"/"),16.0)+input(scan(Obs_GT,2,"/"),16.0);
	Ct_Allele2=2*input(scan(Obs_GT,3,"/"),16.0)+input(scan(Obs_GT,2,"/"),16.0);
	Obs_GT=translate(Obs_GT_Pre,"_","/");
	run;proc sort data=HWE out=HWE_sort nodupkey;by chr pos OBS_GT;run;
	
data lmiss;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\missing.lmiss" 
		dlm='09'x dsd firstobs=2;
	length chrpos $ 20 chr $ 8 ;
	input chr $ pos var1-var2 N_lmiss Freq_lmiss;
	chrpos=cats(chr,"_",pos);
	drop var1-var2;
	index=_n_;
	run;
data SumDepth;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\site_depth.ldepth" 
		dlm='09'x dsd firstobs=2;
	length chrpos $ 20 chr $ 8 ;
	input chr $ pos Sum_Depth Sumsq_Depth;
	chrpos=cats(chr,"_",pos);
	index=_n_;
	drop Sumsq_Depth;
	run;
data MeanDepth;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\site_mean_depth.ldepth.mean" 
		dlm='09'x dsd firstobs=2;
	length chrpos $ 20 chr $ 8 ;
	input chr $ pos Mean_Depth Var_Depth;
	chrpos=cats(chr,"_",pos);
	index=_n_;
	run;
data SiteQual;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\site_quality.lqual" 
		dlm='09'x dsd firstobs=2;
	length chrpos $ 20 chr $ 8 ;
	input chr $ pos Qual;
	chrpos=cats(chr,"_",pos);
	index=_n_;
	run;
data Singleton; /* Not all sites are included */
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\singletons.singletons" 
		dlm='09'x dsd firstobs=2;
	length chrpos $ 20 chr $ 8 pos 8 SinlgeDouble $ 2 Allele_SD $ 2 subject $ 50; 
	input chr $ pos SinlgeDouble $ Allele_SD $ subject $;
	chrpos=cats(chr,"_",pos);
	run;proc sort;by chr pos;run;

data CompleteSet;
	merge Count2 Freq
		  lmiss SumDepth MeanDepth SiteQual;
	by index chr pos;run;
	proc sql;
		create table &type as
		select A.*, B.OBS_GT, "&type" as type length=8 
		from CompleteSet A left join HWE B
		on A.chrpos=B.chrpos;
		quit;run;
data SNV_reordered(rename=(freq_Largest1=MajorAlleleFreq freq_Largest2=MinorAlleleFreq));
	retain chrpos chr pos BiAllelic N_Chr N_Allele N_lmiss Freq_lMiss freq_Largest1 freq_Largest2 Ct_Largest1-Ct_Largest2  
	Obs_GT Mean_Depth Qual
	Freq_Allele1-Freq_Allele4 Ct_Allele1-Ct_Allele4 Allele1-Allele4
	Sum_Depth Var_Depth Type Index;
	set SNV;	
	run;
proc export data=SNV_reordered outfile=
		"\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1/MULTI_SAMPLE/HL/&SampleSet/Summary_Stat/SummaryStat_SNV_&SampleSet..csv"
		dbms=csv replace;run;

%mend;


%GetSNV(SNV,&NumCallset.Callset) 
%GetSNV(SNV,&NumCallset.Callset_&NumMendel.Mendel)
%GetSNV(SNV,&NumCallset.Callset_&NumHapMap.HapMap)






%macro GetINDEL(Type,SampleSet);
data Count2;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\counts2.frq.count" 
		dlm='09'x dsd firstobs=2 missover;
	length chrpos $ 20 chr $ 8;
	input chr $ pos N_Allele N_Chr Ct_Allele1-Ct_Allele7;
	chrpos=cats(chr,"_",pos);
	Ct_largest1=largest(1,of Ct_Allele1-Ct_Allele4);
	Ct_largest2=largest(2,of Ct_Allele1-Ct_Allele4);
	if Ct_Allele3^=. then BiAllelic=0;
		else if Ct_Allele3=. then BiAllelic=1;
	index=_n_;
	run;
	

data Freq;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\freq.frq" 
		dlm='09'x dsd firstobs=2 missover lrecl=10000;
	length chrpos $ 20 chr $ 8 var1-var7 $ 300 Allele1-Allele7 $ 200;
	input chr $ pos N_Allele N_Chr var1-var7 $;
	freq_Allele1=input(scan(var1,2,":"),8.6);freq_Allele2=input(scan(var2,2,":"),8.6);freq_Allele3=input(scan(var3,2,":"),8.6);freq_Allele4=input(scan(var4,2,":"),8.6);
		freq_Allele5=input(scan(var5,2,":"),8.6);freq_Allele6=input(scan(var6,2,":"),8.6);freq_Allele7=input(scan(var7,2,":"),8.6);
	Allele1=scan(var1,1,":");Allele2=scan(var2,1,":");Allele3=scan(var3,1,":");Allele4=scan(var4,1,":");
		Allele5=scan(var5,1,":");Allele6=scan(var6,1,":");Allele7=scan(var7,1,":");
	chrpos=cats(chr,"_",pos);
	Freq_Largest1=largest(1,of Freq_Allele1-Freq_Allele7);
	Freq_Largest2=largest(2,of Freq_Allele1-Freq_Allele7);
	drop N_Allele N_Chr var1-var7;
	index=_n_;
	run;

data HWE; /* For BiAllelic variants only */
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\hardy.hwe" 
		dlm='09'x dsd firstobs=2 missover;
	length chrpos $ 20 chr $ 8 Obs_GT_Pre $ 20 Obs_GT $ 20 Exp_GT $ 20;
	input chr $ pos Obs_GT_Pre $ Exp_GT $ Chisq_HWE P_HWE;
	chrpos=cats(chr,"_",pos);
	Ct_Allele1=2*input(scan(Obs_GT,1,"/"),16.0)+input(scan(Obs_GT,2,"/"),16.0);
	Ct_Allele2=2*input(scan(Obs_GT,3,"/"),16.0)+input(scan(Obs_GT,2,"/"),16.0);
	Obs_GT=translate(Obs_GT_Pre,"_","/");
	run;proc sort data=HWE out=HWE_sort nodupkey;by chr pos OBS_GT;run;
	
data lmiss;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\missing.lmiss" 
		dlm='09'x dsd firstobs=2;
	length chrpos $ 20 chr $ 8 ;
	input chr $ pos var1-var2 N_lmiss Freq_lmiss;
	chrpos=cats(chr,"_",pos);
	drop var1-var2;
	index=_n_;
	run;
data SumDepth;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\site_depth.ldepth" 
		dlm='09'x dsd firstobs=2;
	length chrpos $ 20 chr $ 8 ;
	input chr $ pos Sum_Depth Sumsq_Depth;
	chrpos=cats(chr,"_",pos);
	index=_n_;
	drop Sumsq_Depth;
	run;
data MeanDepth;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\site_mean_depth.ldepth.mean" 
		dlm='09'x dsd firstobs=2;
	length chrpos $ 20 chr $ 8 ;
	input chr $ pos Mean_Depth Var_Depth;
	chrpos=cats(chr,"_",pos);
	index=_n_;
	run;
data SiteQual;
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\site_quality.lqual" 
		dlm='09'x dsd firstobs=2;
	length chrpos $ 20 chr $ 8 ;
	input chr $ pos Qual;
	chrpos=cats(chr,"_",pos);
	index=_n_;
	run;
data Singleton; /* Not all sites are included */
	infile "\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1\MULTI_SAMPLE\HL\&SampleSet\vcftools_&type\singletons.singletons" 
		dlm='09'x dsd firstobs=2;
	length chrpos $ 20 chr $ 8 pos 8 SinlgeDouble $ 2 Allele_SD $ 2 subject $ 50; 
	input chr $ pos SinlgeDouble $ Allele_SD $ subject $;
	chrpos=cats(chr,"_",pos);
	run;proc sort;by chr pos;run;

data CompleteSet; merge Count2 Freq lmiss SumDepth MeanDepth SiteQual; by index 
chr pos;run; proc sql; create table &type as select A.*, B.OBS_GT, "&type" as 
type length=8 from CompleteSet A left join HWE B on A.chrpos=B.chrpos; quit;run;

data &Type._reordered(rename=(freq_Largest1=MajorAlleleFreq freq_Largest2=MinorAlleleFreq));
	retain chrpos chr pos BiAllelic N_Chr N_Allele N_lmiss Freq_lMiss freq_Largest1 freq_Largest2 Ct_Largest1-Ct_Largest2  
	Obs_GT Mean_Depth Qual
	Freq_Allele1-Freq_Allele7 Ct_Allele1-Ct_Allele7 Allele1-Allele7
	Sum_Depth Var_Depth Type Index;
	set &Type;	
	run;

proc export data=&Type._reordered outfile=
		"\\jhgmnt.jhgenomics.jhu.edu\research\active\M_Valle_MD_SeqWholeExome_120417_1/MULTI_SAMPLE/HL/&SampleSet/Summary_Stat/SummaryStat_INDEL_&SampleSet..csv"
		dbms=csv replace;run;
%mend;

%GetINDEL(INDEL,&NumCallset.Callset) 
%GetINDEL(INDEL,&NumCallset.Callset_&NumMendel.Mendel)
%GetINDEL(INDEL,&NumCallset.Callset_&NumHapMap.HapMap)
